import { Observable, Subscription }       from 'rxjs/Rx';
import { Component, OnInit }              from '@angular/core';
import { Store }                          from '@ngrx/store';
import { ActivatedRoute, Params, Router } from '@angular/router';

import {
  FETCH_POSTS,
} from '../state-management/actions/posts';

import {
  FETCH_SUB_SUGGESTIONS,
} from '../state-management/actions/subs';

import { PostListState } from '../state-management/reducers/post-list';
import { SubsState }     from '../state-management/reducers/subs';

@Component({
  selector: 'sub-listing-container',
  template: `
    <div class="sub-listing-container" fxLayout="column" fxLayoutGap="1em">
      <sub-chooser [suggestions]="(subs$ | async).suggestions" (onChange)="onSubChange($event)" (onSuggest)="onSubSuggest($event)"></sub-chooser>
      <div *ngIf="(post_list$ | async).error" class="alert alert-danger">{{(post_list$ | async).error}}</div>
      <div *ngIf="(post_list$ | async).fetching">
        <md-progress-spinner color="primary" mode="indeterminate"></md-progress-spinner>
        <span>Fetching posts from {{currentSub}}...</span>
      </div>
      <post-list
        *ngIf="!(post_list$ | async).fetching && !(post_list$ | async).error"
        [posts]="(post_list$ | async).posts"
        [title]="'List of posts in /r/' + currentSub"
      >
      </post-list>
    </div>
  `,
})
export class SubListingContainer implements OnInit {

  // Observable for selections from Store in constructor()
  post_list$:Observable<PostListState>;
  subs$:Observable<SubsState>;

  // Subscription for route parameters
  params_sub:Subscription;

  // The current sub to list
  currentSub:string = '';

  constructor(

    // Inject Router in order to redirect
    private _router:Router,

    // Inject ActivatedRoute in order to subscribe to route params
    private _route:ActivatedRoute,

    // Inject Store so that state subscriptions can be made and so that actions
    // can be dispatched
    private _store:Store<any>

  ){
    // Store::select() returns an Observable for a particular top-level reducer
    // in the root reducer. The Observable is already configured to drop
    // duplicates (via distinctUntilChanged()).
    //
    // The Observable is actually a BehaviorSubject meaning that it will always
    // emit the current state once subscribed to, as opposed to when it first
    // changes (as with a regular Observable).
    //
    // "post_list" is selected from the Store (as posts$ to follow convention)
    // and AsyncPipe in used in templates to retrieve the value (i.e. "{{posts$
    // | async}}")
    this.post_list$ = _store.select('post_list');
    this.subs$      = _store.select('subs');
  }

  ngOnInit() {
    this.params_sub = this._route.params
      .subscribe((params) => {
        this.currentSub = params['sub'];
        this.getPosts(this.currentSub);
      });
  }

  ngOnDestroy() {
    this.params_sub.unsubscribe();
  }

  onSubChange(newSub:string):void {
    this._router.navigate(['/sub-listing', newSub]);
  }

  onSubSuggest(query:string):void {
    this._store.dispatch({type: FETCH_SUB_SUGGESTIONS, payload: {text: query, max: 5}});
  }

  getPosts(sub:string):void {
    // Dispatch "FETCH_POSTS" action with the "sub" name. The PostEffects
    // service will handle asynchronous fetching via PostsService and will
    // dispatch SET_POSTS or FETCH_POSTS_FAILED accordingly.
    this._store.dispatch({type: FETCH_POSTS, payload: sub});
  }
}
