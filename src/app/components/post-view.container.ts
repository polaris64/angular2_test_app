import { Observable, Subscription }       from 'rxjs/Rx';
import { Component, OnInit }              from '@angular/core';
import { Store }                          from '@ngrx/store';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { PostListState } from '../state-management/reducers/post-list';

import { Post } from '../types/Post';

import {
  FETCH_SINGLE_POST
} from '../state-management/actions/posts';

@Component({
  selector: 'post-view-container',
  template: `
    <div class="post-view-container">
      <div *ngIf="(post$ | async).fetching">Fetching post...</div>
      <div *ngIf="(post$ | async).error" class="alert alert-danger">{{(post$ | async).error}}</div>
      <single-post *ngIf="(post$ | async).data" [singlePost]="(post$ | async).data"></single-post>
    </div>
  `,
})
export class PostViewContainer implements OnInit {

  // Subscription for route parameters
  sub_postPermalink:Subscription;

  // Post being viewed
  post$:Observable<Post>;

  constructor(
    private _router:Router,
    private _route:ActivatedRoute,
    private _store:Store<any>

  ){
    this.post$ = _store.select('single_post');
  }

  ngOnInit() {
    this.sub_postPermalink = Observable.combineLatest(
      this._route.params,
      this._store.select('post_list'),
      (params, post_list:PostListState) => {
        const posts:Post[] = post_list.posts.filter((v) => v.id === params['id']);
        return {
          permalink:   posts.length > 0 ? posts[0].permalink : null,
          current_sub: post_list.current_sub,
        };
      }
    )
      .subscribe((res:any) => {

        // Fetch the post if the permalink was found from the post ID
        if (res.permalink)
          this._store.dispatch({type: FETCH_SINGLE_POST, payload: res.permalink});

        // Otherwise, redirect back to the sub listing for the current sub
        else
          this._router.navigate(['/sub-listing', res.current_sub ? res.current_sub : 'angular']);
      });
  }
}
