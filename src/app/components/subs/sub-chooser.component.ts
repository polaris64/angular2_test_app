import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup }                                          from '@angular/forms';
import { Store }                                                           from '@ngrx/store';
import { Observable }                                                      from 'rxjs/Observable';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { Sub } from '../../types/Sub';

@Component({
  selector: 'sub-chooser',
  template: `
    <form class="sub-chooser form-group" fxLayout="row" fxLayout.xs="column" fxLayoutGap="1em" fxLayoutAlign="space-between center" [formGroup]="subForm" novalidate>

      <h3 fxFlexAlign="start" fxHide.xs="true">Choose subreddit:</h3>
      <md-input-container fxFlex="grow" fxFlexFill>
        <input mdInput placeholder="Subreddit name" formControlName="subName" [mdAutocomplete]="auto" />
      </md-input-container>
      <button md-button (click)="onSelect()">Go!</button>

      <md-autocomplete #auto="mdAutocomplete">
        <md-option *ngFor="let sub of suggestions" [value]="sub.name">{{sub.name}}</md-option>
      </md-autocomplete>

    </form>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class SubChooser {
  @Input()  suggestions:Sub[];
  @Output() onChange  = new EventEmitter<string>();
  @Output() onSuggest = new EventEmitter<string>();

  subForm = new FormGroup({
    subName: new FormControl(''),
  });

  constructor(
    private _store:Store<any>
  ) {
    this.subForm.get('subName')
      .valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe(query => {
        if (query.trim().length > 2)
          this.onSuggest.emit(this.subForm.get('subName').value);
      });
  }

  onSelect():void {
    this.onChange.emit(this.subForm.get('subName').value);
  }
}
