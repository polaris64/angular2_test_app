import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Post } from '../../types/Post';

@Component({
  selector: 'post-list-item',
  template: `
    <a md-list-item *ngIf="post" [routerLink]="['/view-post', post.id]">
      <img *ngIf="post.thumbnail" md-list-avatar [src]="post.thumbnail" [alt]="'Post ' + post.id + ' thumbnail'" />
      <span class="badge pull-right">{{post.score}}</span>
      <h4 md-line>&quot;{{post.title}}&quot;</h4>
      <p md-line><label>by:</label> {{post.author}}</p>
    </a>
  `,

  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class PostListItemComponent {
  @Input() post:Post;
}
