import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { SinglePost } from '../../types/SinglePost';

@Component({
  selector: 'single-post',
  template: `
    <md-card *ngIf="singlePost && singlePost.post">
      
      <md-card-title-group>
        <img md-card-md-image *ngIf="singlePost.post.thumbnail" [src]="singlePost.post.thumbnail" [alt]="'Post ' + singlePost.post.id + ' thumbnail'" />
        <md-card-title>&quot;{{singlePost.post.title}}&quot;</md-card-title>
        <md-card-subtitle><strong>by:</strong> {{singlePost.post.author}}, <strong>scrore: </strong>{{singlePost.post.score}}</md-card-subtitle>
      </md-card-title-group>

      <md-card-content *ngIf="singlePost.post.body">
        {{singlePost.post.body}}
      </md-card-content>

      <md-card-actions>
        <a md-fab *ngIf="singlePost.post.url" [href]="singlePost.post.url" target="_blank"><i class="material-icons">link</i></a>
      </md-card-actions>

    </md-card>

    <div fxLayout="column" *ngIf="singlePost.comments && singlePost.comments.length > 0">
      <hr />
      <h4>Comments:</h4>
      <md-card *ngFor="let comment of singlePost.comments">
        <md-card-title>{{comment.author}}</md-card-title>
        <md-card-content>{{comment.text}}</md-card-content>
      </md-card>
    </div>
  `,

  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class SinglePostComponent {
  @Input() singlePost:SinglePost;
}
