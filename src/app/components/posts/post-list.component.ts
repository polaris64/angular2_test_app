import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Post } from '../../types/Post';

@Component({
  selector: 'post-list',
  template: `
    <div *ngIf="!posts || posts.length === 0">
      <h3>No posts to display</h3>
    </div>

    <div *ngIf="posts && posts.length > 0">
      <h3 *ngIf="title">{{title}}</h3>
      <md-nav-list>
        <post-list-item *ngFor="let post of posts" [post]="post"></post-list-item>
      </md-nav-list>
    </div>
  `,

  // As this component is now "dumb", Angular can be told only to update this
  // component hierarchy when a change to a prop occurs. This improves
  // performance by removing unnecessary Angular change detection checks.
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class PostListComponent {
  @Input() posts: Post[];
  @Input() title: string;
}
