import { Component } from '@angular/core';

@Component({
  selector: 'post-view-container',
  template: `
    <div class="page-not-found-container">
      <h3>The requested page was not found</h3>
      <p>
        <a [routerLink]="['/sub-listing', 'angular']">Return home</a>
      </p>
    </div>
  `,
})
export class PageNotFoundContainer { }
