import { Observable }        from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { Store }             from '@ngrx/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  sub:string   = 'angular';
  title:string = 'Angular2 Test: Simple Reddit Client';

  constructor(
  ){ }

  ngOnInit() {
  }
}
