import { Routes } from '@angular/router';

import { SubListingContainer }   from './components/sub-listing.container';
import { PostViewContainer }     from './components/post-view.container';
import { PageNotFoundContainer } from './components/page-not-found.container';

export const appRoutes:Routes = [
  {path: 'sub-listing/:sub', component: SubListingContainer},
  {path: 'view-post/:id',    component: PostViewContainer},

  {path: '', redirectTo: '/sub-listing/angular', pathMatch: 'full'},

  {path: '**', component: PageNotFoundContainer},
]
