import 'hammerjs';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule }           from '@angular/platform-browser';
import { EffectsModule }           from '@ngrx/effects';
import { FlexLayoutModule }        from '@angular/flex-layout';
import { ReactiveFormsModule }     from '@angular/forms';
import { HttpModule }              from '@angular/http';
import { NgModule }                from '@angular/core';
import { RouterModule }            from '@angular/router';
import { StoreModule }             from '@ngrx/store';

// Import Angular Material components
import {
  MdAutocompleteModule,
  MdButtonModule,
  MdCardModule,
  MdInputModule,
  MdListModule,
  MdProgressSpinnerModule,
  MdToolbarModule,
} from '@angular/material';

import { appRoutes } from './app.routes';

import { store, StoreState } from './state-management/store';

import { PostEffects } from './state-management/effects/post.effects';
import { SubEffects }  from './state-management/effects/sub.effects';

import { PostsService } from './services/posts.service';
import { SubsService }  from './services/subs.service';

import { AppComponent }          from './app.component';
import { PageNotFoundContainer } from './components/page-not-found.container';
import { PostViewContainer }     from './components/post-view.container';
import { SubListingContainer }   from './components/sub-listing.container';
import { PostListComponent }     from './components/posts/post-list.component';
import { PostListItemComponent } from './components/posts/post-list-item.component';
import { SinglePostComponent }   from './components/posts/single-post.component';
import { SubChooser }            from './components/subs/sub-chooser.component';

@NgModule({
  declarations: [

    // Root component
    AppComponent,

    // Containers (smart components with knowledge of Store)
    PageNotFoundContainer,
    PostViewContainer,
    SubListingContainer,

    // Dumb components
    PostListComponent,
    PostListItemComponent,
    SinglePostComponent,
    SubChooser,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,

    // Angular Material components
    MdAutocompleteModule,
    MdButtonModule,
    MdCardModule,
    MdInputModule,
    MdListModule,
    MdProgressSpinnerModule,
    MdToolbarModule,

    // Import all effect services and run them through EffectsModule
    EffectsModule.run(PostEffects),
    EffectsModule.run(SubEffects),

    HttpModule,
    ReactiveFormsModule,

    RouterModule.forRoot(appRoutes),

    // Import the Store by providing the root reducer (which is a combination
    // of other reducers, an object with keys matching the reducer name (e.g.
    // "post_list") and values being the reducer functions themselves).
    StoreModule.provideStore(store),

  ],
  providers: [
    PostsService,
    SubsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
