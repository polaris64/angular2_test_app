import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

import { Injectable }      from '@angular/core';
import { Action }          from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';
import { Observable }      from 'rxjs/Observable';

import { SubsService } from '../../services/subs.service';

import { Sub } from '../../types/Sub';

import {
  FETCH_SUB_SUGGESTIONS,
  FETCH_SUB_SUGGESTIONS_FAILED,
  SET_SUB_SUGGESTIONS,
} from '../actions/subs';

@Injectable()
export class SubEffects {
  constructor(
    private subsService:SubsService,
    private actions$:Actions
  ) { }

  @Effect() fetchSubSuggestions$ = this.actions$
    .ofType(FETCH_SUB_SUGGESTIONS)
    .map((action:Action):string => action.payload)
    .switchMap((query:any):Observable<Action> =>

      this.subsService.getSubSuggestions(query.text)
        .map((res:Sub[]):Action => ({type: SET_SUB_SUGGESTIONS, payload: res.filter((_, i) => i < query.max)}))
        .catch((err:string):Observable<Action> =>
          Observable.of({
            type: FETCH_SUB_SUGGESTIONS_FAILED,
            payload: `Unable to fetch suggestions for query "${query}": ${err.toString()}`,
          })
        )

    );
}
