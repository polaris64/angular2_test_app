import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

import { Injectable }      from '@angular/core';
import { Action }          from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';
import { Observable }      from 'rxjs/Observable';

import { PostsService } from '../../services/posts.service';

import { Post }       from '../../types/Post';
import { SinglePost } from '../../types/SinglePost';

import {
  FETCH_POSTS,
  FETCH_POSTS_FAILED,
  FETCH_SINGLE_POST,
  FETCH_SINGLE_POST_FAILED,
  SET_POSTS,
  SET_SINGLE_POST,
} from '../actions/posts';

@Injectable()
export class PostEffects {
  constructor(
    private postsService:PostsService,

    // Actions is an Observable<Action> which receives every Action dispatched
    // to the store
    private actions$:Actions
  ) { }


  // An Effect needs to listen to one or more Action and return an
  // Observable<Action> which will be dispatched to the store. Any asynchronous
  // operations can occur between receiving the Action and returning the output
  // Observable<Action>.
  @Effect() fetchPosts$ = this.actions$

    // Filter for FETCH_POSTS action
    .ofType(FETCH_POSTS)

    // Retrieve the "sub" from the Action payload
    .map((action:Action):string => action.payload)

    // Replace Observable<Action> with another returned from
    // PostsService::getPosts() (Observable<Post[]> from PostsService will be
    // mapped into Observable<Action> with an appropriate Action instance).
    .switchMap((sub:string):Observable<Action> =>

      // Get Observable<Post[]> from PostsService::getPosts()
      this.postsService.getPosts(sub)

        // Return an Observable<Action> for "SET_POSTS"
        .map((res:Post[]):Action => ({type: SET_POSTS, payload: res}))

        // Any errors from the service will result in Observable<Action> for
        // "FETCH_POSTS_FAILED".
        .catch((err:string):Observable<Action> =>
          Observable.of({
            type: FETCH_POSTS_FAILED,
            payload: `Unable to fetch posts from "${sub}": ${err.toString()}`,
          })
        )

    );


  @Effect() fetchSinglePost$ = this.actions$
    .ofType(FETCH_SINGLE_POST)
    .map((action:Action):string => action.payload)
    .switchMap((permalink:string) =>
      this.postsService.getPost(permalink)
        .map((res:SinglePost):Action => ({type: SET_SINGLE_POST, payload: res}))
        .catch((err:string):Observable<Action> =>
          Observable.of({
            type: FETCH_SINGLE_POST_FAILED,
            payload: `Unable to fetch post from "${permalink}": ${err.toString()}`,
          })
        )
    );
}
