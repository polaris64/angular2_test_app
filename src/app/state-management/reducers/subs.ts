import { Action, ActionReducer } from '@ngrx/store';

import { Sub } from '../../types/Sub';

import {
  CLEAR_SUGGESTIONS,
  FETCH_SUB_SUGGESTIONS,
  FETCH_SUB_SUGGESTIONS_FAILED,
  SET_SUB_SUGGESTIONS,
} from '../actions/subs';

export interface SubsState {
  error:string;
  fetching:boolean;
  suggestions:Sub[];
};

const defaultState:SubsState = {
  error:       null,
  fetching:    false,
  suggestions: [],
};

const error = (state:string = null, action:Action):string => {
  switch (action.type)
  {
    case CLEAR_SUGGESTIONS:
    case FETCH_SUB_SUGGESTIONS:
    case SET_SUB_SUGGESTIONS:
      return null;

    case FETCH_SUB_SUGGESTIONS_FAILED:
      return action.payload;

    default:
      return state;
  }
};

const fetching = (state:boolean = false, action:Action):boolean => {
  switch (action.type)
  {
    case FETCH_SUB_SUGGESTIONS:
      return true;

    case CLEAR_SUGGESTIONS:
    case FETCH_SUB_SUGGESTIONS_FAILED:
    case SET_SUB_SUGGESTIONS:
      return false;

    default:
      return state;
  }
};

const suggestions = (state:Sub[] = [], action:Action):Sub[] => {
  switch (action.type)
  {
    case CLEAR_SUGGESTIONS:
      return [];

    case SET_SUB_SUGGESTIONS:
      return action.payload.slice();

    default:
      return state;
  }
};

export const subs = (state:SubsState = defaultState, action:Action):SubsState => {
  switch (action.type)
  {
    case CLEAR_SUGGESTIONS:
    case FETCH_SUB_SUGGESTIONS:
    case FETCH_SUB_SUGGESTIONS_FAILED:
    case SET_SUB_SUGGESTIONS:
      return Object.assign({}, state, {
        error:       error(state.error,             action),
        fetching:    fetching(state.fetching,       action),
        suggestions: suggestions(state.suggestions, action),
      });
    default:
      return state;
  }
}
