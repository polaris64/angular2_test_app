/* 
 * Action is defined as: -
 *  interface Action {
 *    type: string;
 *    payload?: any;
 *  }
 *
 * ActionReducer is defined as: -
 *  interface ActionReducer<T> {
 *    (state: T, action: Action): T;
 *  }
*/

import { Action, ActionReducer } from '@ngrx/store';

import { Post } from '../../types/Post';

import {
  FETCH_POSTS,
  FETCH_POSTS_FAILED,
  SET_POSTS,
} from '../actions/posts';

export interface PostListState {
  error:string;
  fetching:boolean;
  current_sub:string;
  posts:Post[];
};

const defaultState:PostListState = {
  error:       null,
  fetching:    false,
  current_sub: '',
  posts:       [],
};

const current_sub:ActionReducer<string> = (state:string = defaultState.current_sub, action:Action) => {
  switch (action.type)
  {
    case FETCH_POSTS:
      return action.payload;

    default:
      return state;
  }
};

const error:ActionReducer<string> = (state:string = defaultState.error, action:Action) => {
  switch (action.type)
  {
    case FETCH_POSTS:
    case SET_POSTS:
      return null;

    case FETCH_POSTS_FAILED:
      return action.payload;

    default:
      return state;
  }
};

const fetching:ActionReducer<boolean> = (state:boolean = defaultState.fetching, action:Action) => {
  switch (action.type)
  {
    case FETCH_POSTS:
      return true;

    case FETCH_POSTS_FAILED:
      return false;

    case SET_POSTS:
      return false;

    default:
      return state;
  }
};

const posts:ActionReducer<Post[]> = (state:Post[] = defaultState.posts, action:Action) => {
  switch (action.type)
  {
    case SET_POSTS:
      return action.payload ? action.payload.slice() : [];

    default:
      return state;
  }
};

export const post_list:ActionReducer<PostListState> = (
  state:PostListState = defaultState,
  action:Action
):PostListState => {
  switch (action.type)
  {
    case FETCH_POSTS:
    case FETCH_POSTS_FAILED:
    case SET_POSTS:
      return Object.assign({}, state, {
        current_sub: current_sub(state.current_sub, action),
        error:       error(state.error, action),
        fetching:    fetching(state.fetching, action),
        posts:       posts(state.posts, action),
      });

    default:
      return state;
  }
}

// The post_list reducer above demonstrates delegation of keys to lower-level
// reducers, however a simple reducer like this could simply be implemented as
// follows: -
/*
export const post_list:ActionReducer<PostListState> = (
  state:PostListState = defaultState,
  action:Action
):PostListState => {
  switch (action.type)
  {
    case FETCH_POSTS:
      return Object.assign({}, state, {current_sub: action.payload, fetching: true, error: null});

    case FETCH_POSTS_FAILED:
      return Object.assign({}, state, {fetching: false, error: action.payload});

    case SET_POSTS:
      return Object.assign({}, state, {fetching: false, error: null, posts: action.payload.slice()});

    default:
      return state;
  }
}
*/
