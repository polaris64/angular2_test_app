import { Action, ActionReducer } from '@ngrx/store';

import { SinglePost } from '../../types/SinglePost';

import {
  FETCH_SINGLE_POST,
  FETCH_SINGLE_POST_FAILED,
  SET_SINGLE_POST,
} from '../actions/posts';

export interface SinglePostState {
  error:string;
  fetching:boolean;
  data:SinglePost;
};

const defaultState:SinglePostState = {
  error:    null,
  fetching: false,
  data:     null,
};

const error:ActionReducer<string> = (state:string = defaultState.error, action:Action) => {
  switch (action.type)
  {
    case FETCH_SINGLE_POST:
    case SET_SINGLE_POST:
      return null;

    case FETCH_SINGLE_POST_FAILED:
      return action.payload;

    default:
      return state;
  }
};

const fetching:ActionReducer<boolean> = (state:boolean = defaultState.fetching, action:Action) => {
  switch (action.type)
  {
    case FETCH_SINGLE_POST:
      return true;

    case FETCH_SINGLE_POST_FAILED:
      return false;

    case SET_SINGLE_POST:
      return false;

    default:
      return state;
  }
};

const data:ActionReducer<SinglePost> = (state:SinglePost = defaultState.data, action:Action) => {
  switch (action.type)
  {
    case SET_SINGLE_POST:
      return action.payload ? Object.assign({}, action.payload) : null;

    default:
      return state;
  }
};

export const single_post:ActionReducer<SinglePostState> = (
  state:SinglePostState = defaultState,
  action:Action
):SinglePostState => {
  switch (action.type)
  {
    case FETCH_SINGLE_POST:
    case FETCH_SINGLE_POST_FAILED:
    case SET_SINGLE_POST:
      return Object.assign({}, state, {
        fetching: fetching(state.fetching, action),
        error:    error(state.error, action),
        data:     data(state.data, action),
      });

    default:
      return state;
  }
}
