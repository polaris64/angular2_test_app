export const FETCH_POSTS        = 'FETCH_POSTS';
export const FETCH_POSTS_FAILED = 'FETCH_POSTS_FAILED';
export const SET_POSTS          = 'SET_POSTS';

export const FETCH_SINGLE_POST        = 'FETCH_SINGLE_POST';
export const FETCH_SINGLE_POST_FAILED = 'FETCH_SINGLE_POST_FAILED';
export const SET_SINGLE_POST          = 'SET_SINGLE_POST';
