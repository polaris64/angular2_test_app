import { ActionReducer } from '@ngrx/store';

import { post_list,   PostListState }   from './reducers/post-list';
import { single_post, SinglePostState } from './reducers/post';
import { subs,        SubsState }       from './reducers/subs';

export interface StoreState {
  post_list:ActionReducer<PostListState>;
  single_post:ActionReducer<SinglePostState>;
  subs:ActionReducer<SubsState>;
};

export const store:StoreState = {
  post_list,
  single_post,
  subs,
}
