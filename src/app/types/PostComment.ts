export class PostComment {
  id:     string;
  author: string;
  text:   string;
}
