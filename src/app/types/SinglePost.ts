import { Post }        from './Post';
import { PostComment } from './PostComment';

export class SinglePost {
  post:      Post;
  comments?: PostComment[];
}
