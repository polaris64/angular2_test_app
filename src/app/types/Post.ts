export class Post {
  id:         string;
  title:      string;
  author:     string;
  score:      number;

  thumbnail?: string;
  url?:       string;
  permalink?: string;
  body?:      string;
}
