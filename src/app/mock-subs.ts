import { Sub } from './types/Sub';

export const SUBS:Sub[] = [
  {name: 'Sub #1', description: 'Description of sub #1'},
  {name: 'Sub #2', description: 'Description of sub #2'},
  {name: 'Sub #3', description: 'Description of sub #3'},
  {name: 'Sub #4', description: 'Description of sub #4'},
  {name: 'Sub #5', description: 'Description of sub #5'},
  {name: 'Sub #6', description: 'Description of sub #6'},
];
