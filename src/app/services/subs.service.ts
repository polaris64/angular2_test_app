import 'rxjs/add/operator/map';

import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable}     from 'rxjs/Observable';

import { Sub } from '../types/Sub';

@Injectable()
export class SubsService {

  private baseUrl:string = 'https://www.reddit.com/subreddits/search.json?q=';

  constructor(private http:Http) { }

  getSubSuggestions(query:string):Observable<Sub[]> {
    return this.http.get(this.baseUrl + query)
      .map(res => res.ok ? res.json().data.children.map(this.toSub) as Sub[] : []);
  }

  toSub(item:any):Sub {
    return {
      name:        item.data.display_name,
      description: item.data.public_description,
    };
  }
}
