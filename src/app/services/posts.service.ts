import 'rxjs/add/operator/map';

import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable }    from 'rxjs/Observable';

import { PostComment } from '../types/PostComment';
import { Post }        from '../types/Post';
import { SinglePost }  from '../types/SinglePost';

@Injectable()
export class PostsService {

  private baseUrl_single:string = 'https://www.reddit.com';
  private baseUrl_multi:string = 'https://www.reddit.com/r/';

  constructor(private http:Http) { }

  getPosts(sub:string):Observable<Post[]> {
    return this.http.get(this.baseUrl_multi + sub + '.json')
      .map(res => res.ok ? res.json() : null)
      .map(res => {
        if (!res || !res.data || !res.data.children || res.data.children.length === 0)
          return [];
        return res.data.children.map(this.toPost) as Post[];
      })
  }

  getPost(id:string):Observable<SinglePost> {
    return this.http.get(this.baseUrl_single + id.replace(/\/$/, '') + '.json')
      .map(res => res.ok ? res.json() : null)
      .map(res => {
        if (!res || res.length === 0 || !res[0].data || !res[0].data.children || res[0].data.children.length === 0)
          return null;
        return {
          post:     this.toPost(res[0].data.children[0]),
          comments: res.length > 0 && res[1].data && res[1].data.children && res[1].data.children.length > 0 ?
            res[1].data.children.map(this.toComment) as PostComment[] : null,
        };
      });
  }

  toComment(item:any):PostComment {
    return {
      id:     item.data.id,
      author: item.data.author,
      text:   item.data.body,
    }
  }

  toPost(item:any):Post {
    return {
      id:        item.data.id,
      title:     item.data.title,
      author:    item.data.author,
      score:     item.data.score,
      thumbnail: item.data.thumbnail.match(/^http/) ? item.data.thumbnail : null,
      url:       item.data.url,
      permalink: item.data.permalink,
      body:      item.data.selftext,
    };
  }
}
