import { Post } from './types/Post';

export const POSTS:Post[] = [
  {id: '1', title: 'Post #1', author: 'Author #1', thumbnail: null, score: 1, url: 'http://example.com/1'},
  {id: '2', title: 'Post #2', author: 'Author #2', thumbnail: null, score: 2, url: 'http://example.com/2'},
  {id: '3', title: 'Post #3', author: 'Author #3', thumbnail: null, score: 3, url: 'http://example.com/3'},
  {id: '4', title: 'Post #4', author: 'Author #4', thumbnail: null, score: 4, url: 'http://example.com/4'},
  {id: '5', title: 'Post #5', author: 'Author #5', thumbnail: null, score: 5, url: 'http://example.com/5'},
  {id: '6', title: 'Post #6', author: 'Author #6', thumbnail: null, score: 6, url: 'http://example.com/6'},
];
