import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { By }                               from '@angular/platform-browser';
import { DebugElement }                     from '@angular/core';
import { Observable }                       from 'rxjs/Observable';

import { EffectsModule }       from '@ngrx/effects';
import { HttpModule }          from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule }         from '@ngrx/store';

import { POSTS } from './mock-posts';
import { SUBS }  from './mock-subs';

import { Post } from './types/Post';
import { Sub }  from './types/Sub';

import { store } from './state-management/store';

import { PostEffects } from './state-management/effects/post.effects';

import { PostsService } from './services/posts.service';
import { SubsService }  from './services/subs.service';

import { AppComponent }          from './app.component';
import { PostListComponent }     from './components/posts/post-list.component';
import { PostListItemComponent } from './components/posts/post-list-item.component';
import { SubChooser }            from './components/subs/sub-chooser.component';
import { SubSuggestions }        from './components/subs/sub-suggestions.component';

describe('AppComponent', () => {

  let comp:AppComponent;
  let fixture:ComponentFixture<AppComponent>;
  let de:DebugElement;
  let el:HTMLElement;
  let spy_getPosts:any;

  const postsServiceStub = {
    getPosts: (sub:string):Observable<Post[]> => Observable.of(POSTS),
  };

  const subsServiceStub = {
    getSubSuggestions: (query:string):Observable<Sub[]> => Observable.of(SUBS),
  }

  let _title:string = 'Angular2 Test App';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        PostListComponent,
        PostListItemComponent,
        SubChooser,
        SubSuggestions,
      ],
      imports: [
        EffectsModule.run(PostEffects),
        HttpModule,
        ReactiveFormsModule,
        StoreModule.provideStore(store),
      ],
      providers: [

        // Inject real service, methods will be spied upon later
        //PostsService,

        // Provide stubs instead of real services if necessary
        {provide: PostsService, useValue: postsServiceStub},
        {provide: SubsService,  useValue: subsServiceStub},
      ],
    })

      // Asynchronously compile all components (including async loading of
      // templates, etc.). Returns a Promise.
      .compileComponents();

    // Create spy for PostsService::getPosts() which returns mocked POSTS
    // This is not necessary as the PostsService is stubbed above, but this
    // allows tests to check that the method was called
    spy_getPosts = spyOn(
      TestBed.get(PostsService), // Get service from injector
      'getPosts'                 // Method name to spy upon
    )
      .and.returnValue(Observable.of(POSTS));

    // Create AppComponent (returns component test fixture)
    fixture = TestBed.createComponent(AppComponent);

    // Get the AppComponent instance from the fixture
    comp = fixture.componentInstance;

    // Get the DebugElement from the fixture
    de = fixture.debugElement;

    // Get the native DOM element from the DebugElement
    el = de.nativeElement;

  }));

  it('should create the app', async(() => {
    expect(comp).toBeTruthy();
  }));

  it(`should have as title "${_title}"`, async(() => {
    expect(comp.title).toEqual(_title);
  }));

  it('should display any title', async(() => {
    comp.title = 'test2';
    fixture.detectChanges();
    expect(comp.title).toEqual('test2');
  }));

  it('should render title in a h2 tag', async(() => {
    fixture.detectChanges();
    expect(el.querySelector('h2').textContent).toContain(_title);
  }));

  it('should contain .panel, .panel-heading and .panel-body', async(() => {
    fixture.detectChanges();
    expect(el.querySelector('.panel')).toBeTruthy();
    expect(el.querySelector('.panel-heading')).toBeTruthy();
    expect(el.querySelector('.panel-body')).toBeTruthy();
  }));

  it('should call the PostsService::getPosts() method once', async(() => {
    fixture.detectChanges();
    expect(spy_getPosts.calls.any()).toBe(true, 'PostsService::getPosts() was not called');
  }));
});
