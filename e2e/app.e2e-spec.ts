import { Angular2TestProjectPage } from './app.po';

describe('angular2-test-project App', () => {
  let page: Angular2TestProjectPage;

  beforeEach(() => {
    page = new Angular2TestProjectPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
